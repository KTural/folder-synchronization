# Folder Synchronization

## Content

**Program that synchronizes two folders: source and replica.
  The program should maintain a full, identical copy of source folder at replica folder.**

## Tasks
- Synchronization must be one-way: after the synchronization content of the
  replica folder should be modified to exactly match content of the source
  folder.
- Synchronization should be performed periodically
- File creation/copying/removal operations should be logged to a file and to the
  console output.
- Folder paths, synchronization interval and log file path should be provided
using the command line arguments.
- It is undesirable to use third-party libraries that implement folder
synchronization.
- It is allowed (and recommended) to use external libraries implementing other
well-known algorithms. For example, there is no point in implementing yet
another function that calculates MD5 if you need it for the task – it is
perfectly acceptable to use a third-party (or built-in) library.

## Usage

- Running the program with -h or --help to see usage:
    ```sh
    $ python main.py -h
    usage: main.py [-h] source replica interval log

    Folder synchronization
    
    positional arguments:
        source      Path to the source folder
        replica     Path to the replica folder
        interval    Periodical interval for synchronization (in seconds)
        log         Path to the log file
    
    options:
        -h, --help  show this help message and exit
    ```

- Running the progam with sample source folder, replica folder and log files:

    Sample folder structure:
    ```sh
    $ tree -a -C source
    source
    ├── .config
    │   ├── .conf
    │   ├── config.py
    │   ├── new
    │   │   └── init.py
    │   └── .util
    ├── extra
    │   ├── config
    │   │   └── config.py
    │   ├── extra.py
    │   └── op
    │       └── .op.py
    └── main.py
    
    6 directories, 8 files
    ```
    Running the program:
    ```sh
    $ python main.py source replica 60 logfile.txt
    06/16/2023 02:03:46 AM - Created file: replica/./main.py
    Created file: replica/./main.py
    06/16/2023 02:03:46 AM - Copied file: source/main.py -> replica/./main.py
    Copied file: source/main.py -> replica/./main.py
    06/16/2023 02:03:46 AM - Created file: replica/.config/config.py
    Created file: replica/.config/config.py
    06/16/2023 02:03:46 AM - Copied file: source/.config/config.py -> replica/.config/config.py
    Copied file: source/.config/config.py -> replica/.config/config.py
    06/16/2023 02:03:46 AM - Created file: replica/.config/.conf
    Created file: replica/.config/.conf
    06/16/2023 02:03:46 AM - Copied file: source/.config/.conf -> replica/.config/.conf
    Copied file: source/.config/.conf -> replica/.config/.conf
    06/16/2023 02:03:46 AM - Created file: replica/.config/.util
    Created file: replica/.config/.util
    06/16/2023 02:03:46 AM - Copied file: source/.config/.util -> replica/.config/.util
    Copied file: source/.config/.util -> replica/.config/.util
    06/16/2023 02:03:46 AM - Created file: replica/.config/new/init.py
    Created file: replica/.config/new/init.py
    06/16/2023 02:03:46 AM - Copied file: source/.config/new/init.py -> replica/.config/new/init.py
    Copied file: source/.config/new/init.py -> replica/.config/new/init.py
    06/16/2023 02:03:46 AM - Created file: replica/extra/extra.py
    Created file: replica/extra/extra.py
    06/16/2023 02:03:46 AM - Copied file: source/extra/extra.py -> replica/extra/extra.py
    Copied file: source/extra/extra.py -> replica/extra/extra.py
    06/16/2023 02:03:46 AM - Created file: replica/extra/config/config.py
    Created file: replica/extra/config/config.py
    06/16/2023 02:03:46 AM - Copied file: source/extra/config/config.py -> replica/extra/config/config.py
    Copied file: source/extra/config/config.py -> replica/extra/config/config.py
    06/16/2023 02:03:46 AM - Created file: replica/extra/op/.op.py
    Created file: replica/extra/op/.op.py
    06/16/2023 02:03:46 AM - Copied file: source/extra/op/.op.py -> replica/extra/op/.op.py
    Copied file: source/extra/op/.op.py -> replica/extra/op/.op.py
    ```
    Replica folder structure:
    ```sh
    $ tree -a -C replica
    replica
    ├── .config
    │   ├── .conf
    │   ├── config.py
    │   ├── new
    │   │   └── init.py
    │   └── .util
    ├── extra
    │   ├── config
    │   │   └── config.py
    │   ├── extra.py
    │   └── op
    │       └── .op.py
    └── main.py
    
    6 directories, 8 files
    ``` 
    Creating and removing new files/directories in source folder:
    ```sh
    $ touch ./source/extra/op/ops.py && mkdir ./source/extra/config/.usr && rm ./source/.config/new/init.py && rm -r ./source/.config/new
    ```
    Logs in the console output (updated):
    ```sh
    06/16/2023 02:17:46 AM - Created file: replica/extra/op/ops.py
    Created file: replica/extra/op/ops.py
    06/16/2023 02:17:46 AM - Copied file: source/extra/op/ops.py -> replica/extra/op/ops.py
    Copied file: source/extra/op/ops.py -> replica/extra/op/ops.py
    06/16/2023 02:17:46 AM - Removed directory: replica/.config/new
    Removed directory: replica/.config/new
    ```
    Replica folder structure (updated):
    ```sh
    $ tree -a -C replica
    replica
    ├── .config
    │   ├── .conf
    │   ├── config.py
    │   └── .util
    ├── extra
    │   ├── config
    │   │   ├── config.py
    │   │   └── .usr
    │   ├── extra.py
    │   └── op
    │       ├── .op.py
    │       └── ops.py
    └── main.py
    
    6 directories, 8 files
    ``` 
    Creating and removing new files/directories in replica folder:
    ```sh
    $ touch ./replica/tests.py && mkdir ./replica/extra/utils && rm -r ./replica/.config
    ```
    Logs in the console output (updated):
    ```sh
    06/16/2023 02:30:46 AM - Created file: replica/.config/config.py
    Created file: replica/.config/config.py
    06/16/2023 02:30:46 AM - Copied file: source/.config/config.py -> replica/.config/config.py
    Copied file: source/.config/config.py -> replica/.config/config.py
    06/16/2023 02:30:46 AM - Created file: replica/.config/.conf
    Created file: replica/.config/.conf
    06/16/2023 02:30:46 AM - Copied file: source/.config/.conf -> replica/.config/.conf
    Copied file: source/.config/.conf -> replica/.config/.conf
    06/16/2023 02:30:46 AM - Created file: replica/.config/.util
    Created file: replica/.config/.util
    06/16/2023 02:30:46 AM - Copied file: source/.config/.util -> replica/.config/.util
    Copied file: source/.config/.util -> replica/.config/.util
    06/16/2023 02:30:46 AM - Removed file: replica/tests.py
    Removed file: replica/tests.py
    06/16/2023 02:30:46 AM - Removed directory: replica/extra/utils
    Removed directory: replica/extra/utils
    ```
    Replica folder structure (nothing changed due to changes in replica folder):
    ```sh
    $ tree -a -C replica
    replica
    ├── .config
    │   ├── .conf
    │   ├── config.py
    │   └── .util
    ├── extra
    │   ├── config
    │   │   ├── config.py
    │   │   └── .usr
    │   ├── extra.py
    │   └── op
    │       ├── .op.py
    │       └── ops.py
    └── main.py
    
    6 directories, 8 files
    ```
    Log file:
    ```sh
    $ cat logfile.txt
    06/16/2023 02:03:46 AM - Created file: replica/./main.py
    06/16/2023 02:03:46 AM - Copied file: source/main.py -> replica/./main.py
    06/16/2023 02:03:46 AM - Created file: replica/.config/config.py
    06/16/2023 02:03:46 AM - Copied file: source/.config/config.py -> replica/.config/config.py
    06/16/2023 02:03:46 AM - Created file: replica/.config/.conf
    06/16/2023 02:03:46 AM - Copied file: source/.config/.conf -> replica/.config/.conf
    06/16/2023 02:03:46 AM - Created file: replica/.config/.util
    06/16/2023 02:03:46 AM - Copied file: source/.config/.util -> replica/.config/.util
    06/16/2023 02:03:46 AM - Created file: replica/.config/new/init.py
    06/16/2023 02:03:46 AM - Copied file: source/.config/new/init.py -> replica/.config/new/init.py
    06/16/2023 02:03:46 AM - Created file: replica/extra/extra.py
    06/16/2023 02:03:46 AM - Copied file: source/extra/extra.py -> replica/extra/extra.py
    06/16/2023 02:03:46 AM - Created file: replica/extra/config/config.py
    06/16/2023 02:03:46 AM - Copied file: source/extra/config/config.py -> replica/extra/config/config.py
    06/16/2023 02:03:46 AM - Created file: replica/extra/op/.op.py
    06/16/2023 02:03:46 AM - Copied file: source/extra/op/.op.py -> replica/extra/op/.op.py
    06/16/2023 02:17:46 AM - Created file: replica/extra/op/ops.py
    06/16/2023 02:17:46 AM - Copied file: source/extra/op/ops.py -> replica/extra/op/ops.py
    06/16/2023 02:17:46 AM - Removed directory: replica/.config/new
    06/16/2023 02:30:46 AM - Created file: replica/.config/config.py
    06/16/2023 02:30:46 AM - Copied file: source/.config/config.py -> replica/.config/config.py
    06/16/2023 02:30:46 AM - Created file: replica/.config/.conf
    06/16/2023 02:30:46 AM - Copied file: source/.config/.conf -> replica/.config/.conf
    06/16/2023 02:30:46 AM - Created file: replica/.config/.util
    06/16/2023 02:30:46 AM - Copied file: source/.config/.util -> replica/.config/.util
    06/16/2023 02:30:46 AM - Removed file: replica/tests.py
    06/16/2023 02:30:46 AM - Removed directory: replica/extra/utils
    ```
## References
https://docs.python.org/3.11/library/argparse.html#module-argparse

https://docs.python.org/3.11/library/shutil.html

https://docs.python.org/3.11/library/logging.html

https://docs.python.org/3.11/library/os.html


